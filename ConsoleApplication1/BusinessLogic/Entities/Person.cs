﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Person
    { 
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
        public virtual IList<Pet> Pets { get; set; }
    }
}