﻿using System;

namespace Domain.Entities
{
    public class Pet
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual Person Owner { get; set; }
        public virtual Species Species { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
    }
}
