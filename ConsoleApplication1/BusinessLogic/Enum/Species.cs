﻿namespace Domain.Entities
{
    public enum Species
    {
        Cat = 1,
        Dog = 2,
        Fish = 3,
        Horse = 4,
        Hamster = 5
    }
}