﻿using System.Collections.Generic;

namespace BusinessLogic
{
    public interface IRepository<T> 
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void SaveOrUpdate(T entity);
        void Delete(T entity);
    }
}
