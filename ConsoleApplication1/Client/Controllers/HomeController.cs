﻿using BusinessLogic;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using Services;
using System.Reflection;
using System.Web.Mvc;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        private IExampleService _service;

        public HomeController(IExampleService exampleService)
        {
            _service = exampleService;
        }

        public ActionResult Index()
        {
            var people = _service.GetPeopleWithPets();
           
            return View(new PeopleViewModel(people));
        }

        static ISessionFactory GetSessionFactory()
        {
           var _sessionFactory = Fluently.Configure()

           .Database(MsSqlConfiguration.MsSql2012.ConnectionString(@"Data Source=ZOLTANS-LAPTOP\SQlEXPRESS;Initial Catalog=TestDataBase;Integrated Security=True"))

           .Mappings(m =>
           {
               m.FluentMappings.AddFromAssembly(Assembly.Load("DataAccess"));
           })

           .BuildSessionFactory();

            return _sessionFactory;
        }
    }
}