﻿namespace Client
{
    public interface IUnitOfWork
    {
        void BeginTransaction();
        void Commit();
    }
}
