﻿using Domain.Entities;
using System.Collections.Generic;

namespace Client
{
    public class PeopleViewModel
    {
        public IEnumerable<Person> Names { get; }

        public PeopleViewModel(IEnumerable<Person> People)
        {
            Names = People;
        }
    }
}
