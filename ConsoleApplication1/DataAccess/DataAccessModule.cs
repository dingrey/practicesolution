﻿using BusinessLogic;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Domain.Entities;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System.Reflection;

namespace DataAccess
{
    public class DataAccessModule
    {
        public static void Initialise(WindsorContainer container)
        {
            var session = CreateSessionFactory().OpenSession();
            container.Register(Component.For<ISession>().Instance(session));
            container.Register(Component.For<IRepository<Pet>>().ImplementedBy<PetRepository>());
            container.Register(Component.For<IRepository<Person>>().ImplementedBy<PersonRepository>());
        }

        private static ISessionFactory CreateSessionFactory()
        {
            var sessionFactory = Fluently.Configure()
            .Database(MsSqlConfiguration.MsSql2012.ConnectionString(@"Data Source=ZOLTANS-LAPTOP\SQlEXPRESS;Initial Catalog=TestDataBase;Integrated Security=True"))
            .Mappings(m =>
            {
                m.FluentMappings.AddFromAssembly(Assembly.Load("DataAccess"));
            })
            .BuildSessionFactory();

            return sessionFactory;
        }
    }
}
