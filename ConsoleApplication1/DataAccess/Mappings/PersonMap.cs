﻿using Domain.Entities;
using FluentNHibernate.Mapping;

namespace DataAccess.Mappings
{
    public class PersonMap : ClassMap<Person>
    {
        public PersonMap()
        {
            Id(x => x.Id).Column("PersonId");
            Map(x => x.Name).Nullable();
            Map(x => x.Surname);
            Map(x => x.DateOfBirth);
            HasMany(x => x.Pets).Cascade.All();
        }
    }
}
