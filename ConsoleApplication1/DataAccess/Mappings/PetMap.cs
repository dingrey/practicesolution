﻿using Domain.Entities;
using FluentNHibernate.Mapping;

namespace DataAccess.Mappings
{
    public class PetMap : ClassMap<Pet>
    {
        public PetMap()
        {
            Id(x => x.Id).Column("PetId");
            Map(x => x.Name);
            Map(x => x.Species).Column("PetTypeId").CustomType(typeof(Species));
            Map(x => x.DateOfBirth);
            References(x => x.Owner)
                        .Column("PersonId");
        }
    }
}
