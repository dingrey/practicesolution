﻿using BusinessLogic;
using NHibernate;
using System.Collections.Generic;

namespace DataAccess
{
    public class NHibernateRepository<T> : IRepository<T> where T : class
    {
        public ISession _session { get; set; }

        public NHibernateRepository(ISession session)
        {
            _session = session;
        }

        public void SaveOrUpdate(T entity)
        {
            _session.SaveOrUpdate(entity);
        }

        public void Delete(T entity)
        {
            _session.Delete(entity);
        }

        public IEnumerable<T> GetAll()
        {
            return _session.CreateCriteria<T>().List<T>();
        }

        public T GetById(int id)
        {
            return _session.Get<T>(id);
        }
    }
}
