﻿using Domain.Entities;
using NHibernate;

namespace DataAccess
{
    public class PersonRepository : NHibernateRepository<Person>, IPersonRepository
    {
        public PersonRepository(ISession session) : base(session)
        {
        }
    }
}
