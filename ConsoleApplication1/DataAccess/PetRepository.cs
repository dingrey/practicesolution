﻿using Domain.Entities;
using NHibernate;

namespace DataAccess
{
    public class PetRepository : NHibernateRepository<Pet>, IPetRepository
    {
        public PetRepository(ISession session) : base(session)
        {
        }
    }
}
