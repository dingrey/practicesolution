﻿using BusinessLogic;
using DataAccess;
using Domain.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class ExampleService : IExampleService
    {
        IRepository<Person> _personRepository;
        IRepository<Pet> _petRepository;

        public ExampleService(IRepository<Pet> petRepo, IRepository<Person> personRepo)
        {
            _personRepository = personRepo;
            _petRepository = petRepo;
        }

        public void AddPet(int newOwnerId, Species species, string name, DateTime dateOfBirth)
        {
            _petRepository.SaveOrUpdate(new Pet
            {
                Name = name,
                Species = species,
                Owner = _personRepository.GetById(newOwnerId),
                DateOfBirth = dateOfBirth
            });
        }

        public void AddPerson(Person newOwner)
        {
            _personRepository.SaveOrUpdate(newOwner);
        }

        public IEnumerable<Person> GetPeopleWithoutPets()
        {
            return _personRepository.GetAll().Where(p => p.Pets.Count == 0);
        }

        public IEnumerable<Person> GetPeopleWithPets()
        {
            return _personRepository.GetAll().Where(p => p.Pets.Count > 0);
        }
        
        public IEnumerable<Person> GetPeopleWith(Species species)
        {
            return _personRepository.GetAll().Where(p => p.Pets.Any(s => s.Species == species));
        }
    }
}
