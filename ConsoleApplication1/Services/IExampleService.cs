﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Services
{
    public interface IExampleService
    {
        void AddPet(int newOwnerId, Species species, string name, DateTime dateOfBirth);
        void AddPerson(Person newOwner);
        IEnumerable<Person> GetPeopleWithoutPets();
        IEnumerable<Person> GetPeopleWithPets();
    }
}
