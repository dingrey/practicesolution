﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using DataAccess;

namespace Services
{
    public class ServiceModule
    {
        public static void Initialise(WindsorContainer container)
        {
            container.Register(Component.For<IExampleService>().ImplementedBy<ExampleService>());

            DataAccessModule.Initialise(container);
        }
    }
}
